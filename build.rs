#[macro_use]
extern crate clap;
extern crate clap_complete;
extern crate regex;

use clap_complete::{generate_to, Shell};
use std::io::Error;

include!("src/cli.rs");

fn main() -> Result<(), Error> {
    let mut cmd = build_cli();
    let out_dir = std::env::var_os("OUT_DIR").unwrap();
    let path = generate_to(
        Shell::Bash,
        &mut cmd,    // We need to specify what generator to use
        "multitest", // We need to specify the bin name manually
        &out_dir,    // We need to specify where to write to
    )?;
    println!("cargo:warning=bash completion file is generated: {path:?}");

    let path = generate_to(
        Shell::Fish,
        &mut cmd,    // We need to specify what generator to use
        "multitest", // We need to specify the bin name manually
        &out_dir,    // We need to specify where to write to
    )?;
    println!("cargo:warning=fish completion file is generated: {path:?}");

    let path = generate_to(
        Shell::Zsh,
        &mut cmd,    // We need to specify what generator to use
        "multitest", // We need to specify the bin name manually
        &out_dir,    // We need to specify where to write to
    )?;
    println!("cargo:warning=zsh completion file is generated: {path:?}");

    Ok(())
}
