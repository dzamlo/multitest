extern crate atty;
#[macro_use]
extern crate clap;
extern crate glob;
extern crate handlebars;
extern crate itertools;
extern crate regex;
extern crate serde_json;
extern crate termcolor;
extern crate toml;

#[macro_use]
mod eprint;
mod cli;
mod config;
mod helper;
mod test;

use std::process::exit;
use termcolor::ColorChoice;

fn main() {
    let mut matches = cli::build_cli().get_matches();

    let config_file = matches.remove_one("config_file");
    let filter = matches.remove_one("filter");
    let color_choice = match matches.remove_one::<String>("color").unwrap().as_str() {
        "always" => ColorChoice::Always,
        "auto" => {
            if atty::is(atty::Stream::Stderr) {
                ColorChoice::Auto
            } else {
                ColorChoice::Never
            }
        }
        "never" => ColorChoice::Never,
        _ => unreachable!(),
    };

    unsafe {
        eprint::set_color_choice(color_choice);
    }

    let success = match config::run_config_root(config_file, &filter) {
        Ok(result) => {
            result.summary();
            result.is_success()
        }
        Err(()) => false,
    };

    exit(i32::from(!success));
}
