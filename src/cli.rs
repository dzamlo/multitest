use clap::{Arg, Command};
use regex::Regex;

pub fn build_cli() -> Command {
    command!()
        .arg(
            Arg::new("color")
                .long("color")
                .value_name("WHEN")
                .value_parser(["always", "auto", "never"])
                .default_value("auto")
                .help("When to use color in the output"),
        )
        .arg(
            Arg::new("config_file")
                .long("config")
                .value_name("CONFIG_FILE")
                .value_parser(clap::value_parser!(std::path::PathBuf))
                .help("Select a configuration file instead of searching for a multitest.toml file"),
        )
        .arg(
            Arg::new("filter")
                .long("filter")
                .value_name("FILTER")
                .value_parser(|filter: &str| Regex::new(filter).map_err(|e| e.to_string()))
                .help("Only run tests that match the filter"),
        )
}
