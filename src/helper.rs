use handlebars::handlebars_helper;
use handlebars::Handlebars;

handlebars_helper!(lowercase: |s: String| s.to_lowercase());
handlebars_helper!(uppercase: |s: String| s.to_uppercase());

pub fn regsiter_all_helpers(registry: &mut Handlebars) {
    registry.register_helper("lowercase", Box::new(lowercase));
    registry.register_helper("uppercase", Box::new(uppercase));
}
