use crate::helper::regsiter_all_helpers;
use handlebars::Context;
use handlebars::Handlebars;
use handlebars::JsonValue;
use handlebars::RenderContext;
use handlebars::RenderError;
use handlebars::Renderable;
use handlebars::Template;
use handlebars::TemplateError;
use itertools::Itertools;
use serde_json::json;
use std::convert::TryFrom;
use std::fmt;
use std::io;
use std::process::Command;
use std::process::ExitStatus;

#[derive(Clone, Debug)]
pub struct ParsedTest {
    name: Template,
    command: Vec<Template>,
    clear_env: bool,
    env: Vec<(Template, Template)>,
    variables: Vec<(String, Vec<toml::Value>)>,
}

#[derive(Debug)]
pub enum ParseError {
    NotAATable,
    MissingName,
    InvalidName,
    ClearEnvInvalidType,
    MissingCommand,
    EmptyCommand,
    CommandInvalidType,
    CommandInvalidTemplate(String, Box<TemplateError>),
    VariablesInvalidType,
    VariableInvalidType,
    EnvInvalidType,
    EnvInvalid(EnvParseError),
}

impl fmt::Display for ParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ParseError::NotAATable => write!(f, "not a table"),
            ParseError::MissingName => write!(f, "the name was missing"),
            ParseError::InvalidName => write!(f, "the name was invalid"),
            ParseError::ClearEnvInvalidType => write!(f, "clear_env was not a boolean"),
            ParseError::MissingCommand => write!(f, "the command was missing"),
            ParseError::EmptyCommand => write!(f, "the command was empty"),
            ParseError::CommandInvalidType => write!(f, "the command was not an array"),
            ParseError::CommandInvalidTemplate(_, template_error) => write!(
                f,
                "one of the command element was an invalid template: {}",
                template_error
            ),
            ParseError::VariablesInvalidType => write!(f, "variables was not a table"),
            ParseError::VariableInvalidType => write!(f, "a variables was not an array"),
            ParseError::EnvInvalidType => write!(f, "the env field was not an array"),
            ParseError::EnvInvalid(env_parse_error) => write!(f, "{}", env_parse_error),
        }
    }
}

#[derive(Debug)]
pub enum EnvParseError {
    NotAATable,
    MissingName,
    InvalidName(Box<TemplateError>),
    InvalidNameType,
    MissingValue,
    InvalidValue(Box<TemplateError>),
    InvalidValueType,
}

impl fmt::Display for EnvParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            EnvParseError::NotAATable => write!(f, "an env was not a table"),
            EnvParseError::MissingName => write!(f, "an env was missing a name"),
            EnvParseError::InvalidName(error) => {
                write!(f, "the name of an env was invalid: {}", error)
            }
            EnvParseError::InvalidNameType => write!(f, "the name of an env was not a string"),
            EnvParseError::MissingValue => write!(f, "an env was missing a value"),
            EnvParseError::InvalidValue(error) => {
                write!(f, "the value of an env was invalid: {}", error)
            }
            EnvParseError::InvalidValueType => write!(f, "the value of an env was not a string"),
        }
    }
}

impl TryFrom<&toml::Value> for ParsedTest {
    type Error = ParseError;

    fn try_from(value: &toml::Value) -> Result<Self, Self::Error> {
        if !value.is_table() {
            return Err(ParseError::NotAATable);
        }

        let table = value.as_table().unwrap();

        let name = table.get("name");
        if name.is_none() {
            return Err(ParseError::MissingName);
        }
        let name = name.unwrap();
        let name = match name
            .as_str()
            .map(|s| Template::compile_with_name(s, "name".to_string()))
        {
            None | Some(Err(_)) => return Err(ParseError::InvalidName),
            Some(Ok(template)) => template,
        };

        let command = table.get("command").map(|c| c.as_array());
        let command_iter = match command {
            None => return Err(ParseError::MissingCommand),
            Some(None) => return Err(ParseError::CommandInvalidType),
            Some(Some(command)) => command.iter().map(|x| x.as_str()),
        };

        let mut command = vec![];
        for e in command_iter {
            match e {
                None => return Err(ParseError::CommandInvalidType),
                Some(str) => match Template::compile(str) {
                    Ok(template) => command.push(template),
                    Err(error) => {
                        return Err(ParseError::CommandInvalidTemplate(
                            str.to_string(),
                            Box::new(error),
                        ))
                    }
                },
            }
        }
        if command.is_empty() {
            return Err(ParseError::EmptyCommand);
        }

        let clear_env = table
            .get("clear_env")
            .unwrap_or(&toml::Value::Boolean(false));
        let clear_env = match clear_env.as_bool() {
            None => return Err(ParseError::ClearEnvInvalidType),
            Some(clear_env) => clear_env,
        };

        let (env, error) = match table.get("env") {
            None => (vec![], vec![]),
            Some(value) => match value.as_array() {
                None => return Err(ParseError::EnvInvalidType),
                Some(values) => values.iter().map(try_parse_env).partition(Result::is_ok),
            },
        };

        if !error.is_empty() {
            let error = error.into_iter().map(Result::unwrap_err).next().unwrap();
            return Err(ParseError::EnvInvalid(error));
        }

        let env = env.into_iter().map(Result::unwrap).collect();

        let variables = match table.get("variables") {
            None => vec![],
            Some(map) => match map.as_table() {
                None => return Err(ParseError::VariablesInvalidType),
                Some(map) => {
                    let mut variables = vec![];
                    for (name, value) in map.iter() {
                        match value.as_array() {
                            None => return Err(ParseError::VariableInvalidType),
                            Some(values) => variables.push((name.clone(), values.clone())),
                        }
                    }
                    variables
                }
            },
        };

        Ok(ParsedTest {
            name,
            command,
            clear_env,
            env,
            variables,
        })
    }
}

fn try_parse_env(value: &toml::Value) -> Result<(Template, Template), EnvParseError> {
    match value.as_table() {
        None => Err(EnvParseError::NotAATable),
        Some(table) => match table.get("name") {
            None => Err(EnvParseError::MissingName),
            Some(name) => match name.as_str().map(Template::compile) {
                Some(Ok(name)) => match table.get("value") {
                    None => Err(EnvParseError::MissingValue),
                    Some(value) => match value.as_str().map(Template::compile) {
                        Some(Ok(value)) => Ok((name, value)),
                        Some(Err(template_error)) => {
                            Err(EnvParseError::InvalidValue(Box::new(template_error)))
                        }
                        None => Err(EnvParseError::InvalidValueType),
                    },
                },
                Some(Err(template_error)) => {
                    Err(EnvParseError::InvalidName(Box::new(template_error)))
                }
                None => Err(EnvParseError::InvalidNameType),
            },
        },
    }
}

impl ParsedTest {
    pub fn gen_arg_matrices(&self) -> Vec<Vec<(String, toml::Value)>> {
        if self.variables.is_empty() {
            return vec![vec![]];
        }

        let remaining_variables = &self.variables[..];
        gen_arg_matrices_inner(vec![], remaining_variables)
    }

    pub fn gen_test_instances(&self) -> Result<Vec<TestInstance>, RenderError> {
        let mut registry = Handlebars::new();
        regsiter_all_helpers(&mut registry);
        self.gen_arg_matrices()
            .into_iter()
            .map(|variables| self.gen_test_instance(&registry, &variables[..]))
            .collect()
    }

    fn gen_test_instance(
        &self,
        registry: &Handlebars,
        variables: &[(String, toml::Value)],
    ) -> Result<TestInstance, RenderError> {
        let mut map = serde_json::Map::new();
        for (name, value) in variables {
            let value = json!(value);
            map.insert(name.to_string(), value);
        }
        let mut ctx = Context::from(serde_json::Value::Object(map));
        // We create a clone to borrow it while still being able to borrow it mutably to insert the name into it.
        let ctx2 = ctx.clone();
        let mut render_context = RenderContext::new(None);

        let name = self.name.renders(registry, &ctx2, &mut render_context)?;

        let map = ctx.data_mut();
        let map = map.as_object_mut().unwrap();
        map.insert("name".to_string(), JsonValue::String(name.clone()));

        let mut command = vec![];
        for tpl in &self.command {
            command.push(tpl.renders(registry, &ctx, &mut render_context)?);
        }

        let mut env = vec![];
        for (name, value) in &self.env {
            let name = name.renders(registry, &ctx, &mut render_context)?;
            let value = value.renders(registry, &ctx, &mut render_context)?;
            env.push((name, value))
        }

        Ok(TestInstance {
            name,
            clear_env: self.clear_env,
            command,
            env,
        })
    }
}

fn gen_arg_matrices_inner(
    current_value: Vec<(String, toml::Value)>,
    remaining_variables: &[(String, Vec<toml::Value>)],
) -> Vec<Vec<(String, toml::Value)>> {
    if remaining_variables.is_empty() {
        vec![current_value]
    } else {
        let current_variable = &remaining_variables[0];
        let remaining_variables = &remaining_variables[1..];
        let mut result = vec![];
        for value in &current_variable.1 {
            let mut current_value = current_value.clone();
            current_value.push((current_variable.0.clone(), value.clone()));
            for foo in gen_arg_matrices_inner(current_value, remaining_variables) {
                result.push(foo);
            }
        }
        result
    }
}

#[derive(Debug)]
pub struct TestInstance {
    pub name: String,
    pub clear_env: bool,
    pub command: Vec<String>,
    pub env: Vec<(String, String)>,
}

impl TestInstance {
    // Run the command of the test. Doesn't handle the working directory.
    fn run_command(&self) -> io::Result<ExitStatus> {
        let mut command = Command::new(&self.command[0]);
        command.args(&self.command[1..]);

        if self.clear_env {
            command.env_clear();
        }

        for (key, value) in &self.env {
            command.env(key, value);
        }

        command.status()
    }

    pub fn run(&self) -> bool {
        eprintln_bold!(
            "Running test {} ({})",
            self.name.escape_debug(),
            self.command.iter().map(|s| s.escape_debug()).format(" ")
        );

        let command_result = self.run_command();

        match command_result {
            Err(error) => {
                eprintln_red!(
                    "Test {} failed: {}",
                    self.name.escape_debug(),
                    error.to_string().escape_debug()
                );
                false
            }
            Ok(status) => {
                if status.success() {
                    eprintln_green!("Test {} was successful", self.name.escape_debug());
                    true
                } else {
                    match status.code() {
                        Some(code) => {
                            eprintln_red!(
                                "Test {} failed: exit code {}",
                                self.name.escape_debug(),
                                code
                            );
                        }
                        None => {
                            eprintln_red!("Test {} failed: no exit code", self.name.escape_debug());
                        }
                    }
                    false
                }
            }
        }
    }
}

/*
impl Test {
    fn run_command(&self, registry: Handlebars) -> io::Result<ExitStatus> {
        let mut command = Command::new(&self.command[0]);
        command.args(&self.command[1..]);

        if self.clear_env {
            command.env_clear();
        }

        for &(ref key, ref value) in &self.env {
            command.env(key, value);
        }

        command.status()
    }

    pub fn run(&self) -> bool {
        eprintln_bold!("Running test {} ({})", self.name, self);

        let command_result = self.run_command();

        match command_result {
            Err(error) => {
                eprintln_red!("Test {} failed: {}", self.name, error);
                false
            }
            Ok(status) => {
                if status.success() {
                    eprintln_green!("Test {} was successful", self.name);
                    true
                } else {
                    match status.code() {
                        Some(code) => {
                            eprintln_red!("Test {} failed: exit code {}", self.name, code);
                        }
                        None => {
                            eprintln_red!("Test {} failed: no exit code", self.name);
                        }
                    }
                    false
                }
            }
        }
    }
}
*/
