use crate::test::ParsedTest;
use crate::test::TestInstance;
use glob::glob;
use regex::Regex;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use toml::Value;

const CONFIG_FILE_NAME: &str = "multitest.toml";

#[derive(Default)]
pub struct RunConfigResult {
    ignored: u32,
    successes: Vec<String>,
    failures: Vec<String>,
}

impl RunConfigResult {
    pub fn merge(&mut self, other: RunConfigResult) {
        self.ignored += other.ignored;
        self.successes.extend(other.successes);
        self.failures.extend(other.failures);
    }

    pub fn summary(&self) {
        let ignored = self.ignored;
        let successes = &self.successes;
        let failures = &self.failures;

        let total = successes.len() + failures.len();

        if !successes.is_empty() {
            eprintln_green!("Successes ({}/{}):", successes.len(), total);
            for success in successes {
                eprintln_green!("  {}", success.escape_debug());
            }
        }

        if !failures.is_empty() {
            eprintln_red!("Failures ({}/{}):", failures.len(), total);
            for failure in failures {
                eprintln_red!("  {}", failure.escape_debug());
            }
        }

        if ignored > 0 {
            eprintln_bold!("{} tests ignored", ignored);
        }

        if total == 0 {
            eprintln_red!("No tests executed")
        }
    }

    pub fn is_success(&self) -> bool {
        self.failures.is_empty() && !self.successes.is_empty()
    }
}

struct ParseResult {
    tests: Vec<TestInstance>,
    includes: Vec<PathBuf>,
}

pub fn find_config_file() -> Option<PathBuf> {
    let current_dir = env::current_dir().unwrap();
    let mut current = &*current_dir;

    loop {
        let config_file = current.join(CONFIG_FILE_NAME);
        if config_file.metadata().is_ok() {
            return Some(config_file);
        }

        match current.parent() {
            Some(parent) => {
                current = parent;
            }
            None => {
                break;
            }
        }
    }

    None
}

fn parse_config(config_filename: &Path) -> Result<ParseResult, ()> {
    let mut config_file = match File::open(config_filename) {
        Ok(file) => file,
        Err(error) => {
            eprintln_red!("Cannot open {}: {}", config_filename.display(), error);
            return Err(());
        }
    };

    let mut config_text = String::new();

    if let Err(error) = config_file.read_to_string(&mut config_text) {
        eprintln_red!(
            "Error while reading {}: {}",
            config_filename.display(),
            error
        );
        return Err(());
    }

    let config_parsed = match config_text.parse::<Value>() {
        Ok(config) => config,
        Err(error) => {
            eprintln_red!(
                "Error while parsing {}: {}",
                config_filename.display(),
                error
            );
            return Err(());
        }
    };

    let mut collected_tests = vec![];

    if let Some(tests) = config_parsed.get("tests").and_then(Value::as_array) {
        for test in tests {
            match ParsedTest::try_from(test) {
                Ok(test) => match test.gen_test_instances() {
                    Ok(mut tests) => collected_tests.append(&mut tests),
                    Err(error) => {
                        eprintln_red!("error while generating tests: {}", error);
                        return Err(());
                    }
                },
                Err(error) => {
                    eprintln_red!("error while parsing test: {}", error);
                    return Err(());
                }
            }
        }
    }

    let mut collected_includes = vec![];

    if let Some(includes) = config_parsed.get("includes").and_then(Value::as_array) {
        for include in includes {
            match Value::as_str(include) {
                Some(include) => match glob(include) {
                    Ok(paths) => {
                        for path in paths {
                            match path {
                                Ok(path) => collected_includes.push(path),
                                Err(err) => {
                                    eprintln_red!("{}", err);
                                    return Err(());
                                }
                            }
                        }
                    }
                    Err(err) => {
                        eprintln_red!("Invalid include glob pattern: {}", err);
                        return Err(());
                    }
                },
                None => {
                    eprintln_red!("includes must be strings");
                    return Err(());
                }
            }
        }
    }

    Ok(ParseResult {
        tests: collected_tests,
        includes: collected_includes,
    })
}

pub fn run_config(config_filename: &Path, filter: &Option<Regex>) -> Result<RunConfigResult, ()> {
    let mut result: RunConfigResult = Default::default();
    let current_dir = match env::current_dir() {
        Ok(current_dir) => current_dir,
        Err(err) => {
            eprintln_red!("cannot get the current working directory: {}", err);
            return Err(());
        }
    };

    let ParseResult { tests, includes } = parse_config(config_filename)?;

    // We move to the directory containing the configuration file. This way tests are always
    // executed from this directory.
    let config_dir = config_filename.parent().unwrap();

    if config_dir.to_str() != Some("") {
        if let Err(error) = env::set_current_dir(config_dir) {
            eprintln_red!(
                "Cannot move the directory containing {}: {}",
                config_filename.display(),
                error
            );
            return Err(());
        }
    }

    for include in &includes {
        eprintln_bold!("Including {}", include.display());
        result.merge(run_config(include, filter)?);
    }

    if !includes.is_empty() {
        eprintln_bold!("Going back to {}", config_filename.display());
    }

    for test in tests {
        if let Some(ref regex) = *filter {
            if !regex.is_match(&test.name) {
                result.ignored += 1;
                eprintln_bold!("Test {} ignored", test.name);
                continue;
            }
        }

        let test_success = test.run();
        if test_success {
            result.successes.push(test.name);
        } else {
            result.failures.push(test.name);
        }
    }

    if let Err(error) = env::set_current_dir(current_dir) {
        eprintln_red!(
            "Cannot move back to the previous working directory: {}",
            error
        );
        return Err(());
    }

    Ok(result)
}

pub fn run_config_root(
    config_filename: Option<PathBuf>,
    filter: &Option<Regex>,
) -> Result<RunConfigResult, ()> {
    let config_filename = match config_filename.or_else(find_config_file) {
        Some(config_filename) => config_filename,
        None => {
            eprintln_red!("{} not found", CONFIG_FILE_NAME);
            return Err(());
        }
    };

    run_config(&config_filename, filter)
}
